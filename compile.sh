#!/bin/bash
SMVERSION=1.10
SMFVERSION=1.10.0-git6482

set -e

mkdir builddir && cd builddir

wget https://sm.alliedmods.net/smdrop/${SMVERSION}/sourcemod-${SMFVERSION}-linux.tar.gz
tar -xzf sourcemod-${SMFVERSION}-linux.tar.gz

cp ../get5_apistats.sp ./addons/sourcemod/scripting/
cp ../include/* ./addons/sourcemod/scripting/include/
cp -r ../get5 ./addons/sourcemod/scripting/

cd addons/sourcemod/scripting/

wget -O include/SteamWorks.inc https://raw.githubusercontent.com/KyleSanderson/SteamWorks/master/Pawn/includes/SteamWorks.inc

git clone https://github.com/clugg/sm-json.git
cd sm-json && git checkout v2.5.3 && cd ..
cp -r sm-json/addons/sourcemod/scripting/include .
rm -rf sm-json

./spcomp64 get5_apistats.sp -o ../../../../get5_apistats.smx
cd ../../../../
rm -rf builddir
